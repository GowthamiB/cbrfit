angular.module("application")
  .controller("circuitCtrl", ["$scope", function ($scope) {
   $scope.showThis = false;
   $scope.isEdited = false;

    $scope.editPreview = function () {
      $scope.showThis = !$scope.showThis;
      $scope.isEdited = !$scope.isEdited;
    }

    $scope.submitThis = function () {
      $scope.showThis = !$scope.showThis;
      $scope.isEdited = !$scope.isEdited;
    }

    $scope.cancelThis = function () {
      $scope.showThis = !$scope.showThis;
      $scope.isEdited = !$scope.isEdited;
    }

  }]);
