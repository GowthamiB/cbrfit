angular.module("application")
  .controller("reportCtrl", ["$scope", function ($scope) {
    $scope.commentsView = false;
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };

    $scope.showComments = function () {
      $scope.commentsView = !$scope.commentsView;
    };

    $scope.hideComments = function () {
      $scope.commentsView = !$scope.commentsView;
    };

    $scope.phases = [{
      "id": 1,
      "name": "Phase1"
    }, {
      "id":2,
      "name": "Phase2"
    }, {
      "id": 3,
      "name": "Phase3"
    }];

  }]);
