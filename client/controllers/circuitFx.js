angular.module("application")
  .controller("fxCtrl", ["$scope", function ($scope) {
   $scope.showThisfx = false;
   $scope.isEditedfx = false;

    $scope.editPreviewfx = function () {
      $scope.showThisfx = !$scope.showThisfx;
      $scope.isEditedfx = !$scope.isEditedfx;
    }

    $scope.submitThisfx = function () {
      $scope.showThisfx = !$scope.showThisfx;
      $scope.isEditedfx = !$scope.isEditedfx;
    }

    $scope.cancelThisfx = function () {
      $scope.showThisfx = !$scope.showThisfx;
      $scope.isEditedfx = !$scope.isEditedfx;
    }

  }]);
