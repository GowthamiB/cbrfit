angular.module("application")
  .controller("HomeCtrl", ["$scope", "$state", "$http", function ($scope, $state, $http) {
    $scope.showMoreOptions = false;
    $scope.circuitBreaker = "";
    $http.get('data/circuitDetails.json').then(function (circuitDetails) {
      $scope.circuitDetails = circuitDetails.data;
      $scope.selectedReport = $scope.circuitDetails[0].reportsData;
    });

    $scope.toggleActions = function (report) {
      report.showMoreOptions = !report.showMoreOptions;
    };

    var pageName = '';
    $scope.showInputs = function (name) {
      event.stopPropagation();
      if(name === "circuitdtcreate") {
        $scope.circuitBreaker = "dt";
      }
      else if(name === "circuitfxcreate"){
        $scope.circuitBreaker = "fx";
      }
      pageName = name;
    };
    $scope.circuitredirect = function () {
      $state.go(pageName);
    };

    $scope.selected = 0;
    $scope.generateReports = function (circuits, index) {
      $scope.selectedReport = circuits.reportsData;
      $scope.selected = index;
    };

    $scope.chart1 = [
      ['req1', 4],
      ['req2', 4],
    ];

    $scope.chart2 = [
      ['req1', 3],
      ['req2', 2]
    ];
    $scope.chart3 = [
      ['req1', 5],
      ['req2', 5],
    ];

    $scope.color1 =['#876aff', '#222222'];

    $scope.color2 = ['#ff9721','#222222'];

    $scope.color3 =['#5fbb66','#222222'];


    $scope.content1 =['11'];

    $scope.content2 = ['13'];

    $scope.content3 =['20'];

    $scope.subcontent1 =['/ 34'];

    $scope.subcontent2 = ['/ 34'];

    $scope.subcontent3 =['/ 34'];

  }])
      .directive('requestChart', function () {
        return {
          restrict: 'C',
          replace: true,
          scope: {
            items: '=',
            colors: '=',
            content: '=',
            subcontent: '='
          },
          controller: function ($scope, $element, $attrs) {
          },
          template: '<div style="margin: 0 auto">not working</div>',
          link: function (scope, element, attrs) {
            var chart = new Highcharts.Chart({
              chart: {
                renderTo: element[0],
                plotBackgroundColor: null,
                plotBorderColor:null,
                plotBorderRadius: '50%',
                borderWidth: 25,
                borderRadius: '50%',
                borderColor:'#222222',
                plotShadow: false,
                width:150,
                backgroundColor: '#181818',
                height:150,
              },
              title: {
                text: scope.content,
                align: 'center',
                verticalAlign: 'middle',
                y:9,
                x:-10,
                style: {
                  color: '#ffffff',
                  fontFamily:'geinspira-bold' ,
                  fontSize:'26px',
                }
              },
              subtitle: {
                text: scope.subcontent,
                align: 'center',
                verticalAlign: 'middle',
                y:9,
                x:18,
                style: {
                  color: '#ffffff',
                  fontFamily:'geinspira-regular' ,
                  fontSize:'14px',
                }
              },
              colors: scope.colors,
              tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage}%</b>',
                percentageDecimals: 1
              },
              plotOptions: {
                pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: false,
                  }
                }
              },
              credits: {
                enabled: false
              },
              series: [{
                type: 'pie',
                innerSize: '90%',
                name: 'Drafts',
                Color: '#181818',
                borderColor: 'transparent',
                data: scope.items
              }]
            });
            scope.$watch("items", function (newValue) {
              chart.series[0].setData(newValue, true);
            }, true);

          }
        }
      });
