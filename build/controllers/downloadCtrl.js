angular.module("application")
  .controller("downloadCtrl", ["$scope", "$state", "$http", function ($scope, $state, $http) {
    $scope.downloadStatus = false;
    $http.get('data/downloadData.json').then(function (downloadData) {
      $scope.downloadData = downloadData.data;
    // This executes when entity in table is checked
    $scope.selectEntity = function () {
      // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
      for (var i = 0; i < $scope.downloadData.length; i++) {
        if (!$scope.downloadData[i].isChecked) {
          $scope.allItemsSelected = false;
          return;
        }
      }

      //If not the check the "allItemsSelected" checkbox
      $scope.allItemsSelected = true;
    };

    // This executes when checkbox in table header is checked
    $scope.selectAll = function () {
      // Loop through all the entities and set their isChecked property
      for (var i = 0; i < $scope.downloadData.length; i++) {
        $scope.downloadData[i].isChecked = $scope.allItemsSelected;
        console.log($scope.downloadData.length);
      }
    };
    });

    $scope.downloadStatusCall = function () {
      $scope.downloadStatus = !$scope.downloadStatus;
    }
  }]);
